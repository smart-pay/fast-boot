package net.maku.framework.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Swagger配置
 *
 * @author 阿沐  babamu@126.com
 */
@Configuration
public class SwaggerConfig{

    @Bean
    public GroupedOpenApi userApi(){
        String[] paths = { "/**" };
        String[] packagedToMatch = { "net.maku" };
        return GroupedOpenApi.builder().group("SmartPay")
                .pathsToMatch(paths)
                .packagesToScan(packagedToMatch).build();
    }

    @Bean
    public OpenAPI customOpenAPI() {
        Contact contact= new Contact();
        contact.setName("华来虎 hualaihu@126.com");

        return new OpenAPI().info(new Info()
            .title("智能付款")
            .description( "智能付款")
            .contact(contact)
            .version("1.0")
            .termsOfService("https://192.168.8.254:8116")
            .license(new License().name("MIT")
            .url("https://192.168.8.254:8116")));
    }

}