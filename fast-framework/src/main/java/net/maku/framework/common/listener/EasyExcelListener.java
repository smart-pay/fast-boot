package net.maku.framework.common.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kj
 */
public class EasyExcelListener extends AnalysisEventListener {
    private List<Object> datas = new ArrayList<>();

    /**
     * 逐行解析
     * @param o 当前行的数据
     * @param analysisContext
     */
    @Override
    public void invoke(Object o, AnalysisContext analysisContext) {
        if (o != null) {
            datas.add(o);
        }
    }
    public List<Object> getDatas() {
        return datas;
    }
    public void setDatas(List<Object> datas) {
        this.datas = datas;
    }
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
    }


}
