package net.maku;

import cn.hutool.core.io.IoUtil;
import net.maku.api.module.storage.StorageService;
import net.maku.framework.common.utils.RedisKeys;
import net.maku.framework.common.utils.RedisUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.nio.file.Files;

@SpringBootTest
public class RedisTest {
    @Autowired
    private RedisUtils redisUtils;

    @Test
    public void getRedis() throws Exception{

        String key="f6a190d1-306f-4c40-b168-4deceb565a77";
        key = RedisKeys.getCaptchaKey(key);
        String captcha = (String)redisUtils.get(key);
        // 删除验证码
        if(captcha != null){
            redisUtils.delete(key);
        }

    }

}
