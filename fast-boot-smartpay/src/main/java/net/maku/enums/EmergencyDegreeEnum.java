package net.maku.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * 紧急级别枚举
 */
@Getter
@AllArgsConstructor
public enum EmergencyDegreeEnum {
    /**
     * 0：普通
     */
    DEGREE_LOWEST(0, "普通"),
    /**
     * 1：加急
     */
    DEGREE_LOWER(1, "加急"),
    /**
     * 2：紧急
     */
    DEGREE_HIGHER(2, "紧急"),
    /**
     * 3：特急
     */
    DEGREE_HIGHEST(3, "特急"),
    ;

    private final Integer value;
    private final String  desc;

    public static Integer getValueFromDesc(String desc){
        Map<String, Integer> map = new HashMap<>(16);
        for (EmergencyDegreeEnum type : values()) {
            map.put(type.getDesc(), type.getValue());
        }

        return map.get(desc);
    }

    public static String getDescFromValue(Integer value){
        Map<Integer, String> map = new HashMap<>(16);
        for (EmergencyDegreeEnum type : values()) {
            map.put(type.getValue(), type.getDesc());
        }

        return map.get(value);
    }
}