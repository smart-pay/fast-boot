package net.maku.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * 审核状态枚举
 */
@Getter
@AllArgsConstructor
public enum AuditStateEnum {
    /**
     * 待审核
     */
    AUDIT_WAIT(0, "待审核"),
    /**
     * 审核通过
     */
    AUDIT_OK(1, "审核通过"),
    /**
     * 审核拒绝
     */
    AUDIT_REFUSE(2, "审核未通过"),
    ;
    private final Integer value;
    private final String  desc;

    public static Integer getValueFromDesc(String desc){
        Map<String, Integer> map = new HashMap<>(16);
        for (AuditStateEnum type : values()) {
            map.put(type.getDesc(), type.getValue());
        }

        return map.get(desc);
    }

    public static String getDescFromValue(Integer value){
        Map<Integer, String> map = new HashMap<>(16);
        for (AuditStateEnum type : values()) {
            map.put(type.getValue(), type.getDesc());
        }

        return map.get(value);
    }
}