package net.maku.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * 支付状态枚举
 */
@Getter
@AllArgsConstructor
public enum PayStateEnum {
    /**
     * 未付
     */
    PAY_WAIT(0, "否"),
    /**
     * 已付
     */
    PAY_FINISH(1, "是"),
    /**
     * 不用付
     */
    PAY_NEEDLESS(2, "不用付"),
    ;

    private final Integer value;
    private final String  desc;

    public static Integer getValueFromDesc(String desc){
        Map<String, Integer> map = new HashMap<>(16);
        for (PayStateEnum type : values()) {
            map.put(type.getDesc(), type.getValue());
        }

        return map.get(desc);
    }

    public static String getDescFromValue(Integer value){
        Map<Integer, String> map = new HashMap<>(16);
        for (PayStateEnum type : values()) {
            map.put(type.getValue(), type.getDesc());
        }

        return map.get(value);
    }
}