package net.maku.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class SmartPayTemplateData implements Serializable {

    private String sn;

    private String seq;

    private String supplier;

    private String projectName;

    private String materialName;

    private String totalPriceDesc;

    private String payStateDesc;

    private String emergencyDegreeDesc;

    private String remark;

}
