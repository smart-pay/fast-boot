package net.maku.excel;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.maku.framework.common.entity.NameBaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 已付款+应付款管理
 *
 * @author 中国华 hualaihu@126.com
 * @since 1.0-SNAPSHOT 2022-08-29
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class PayItemEntity extends NameBaseEntity {
	/**
	* 审批编号
	*/
	private String seq;

	/**
	* 项目名称
	*/
	private String projectName;

	/**
	* 供应商名称
	*/
	private String supplier;

	/**
	* 材料名称
	*/
	private String materialName;

	/**
	* 应付款
	*/
	private BigDecimal totalPrice;

	/**
	* 紧急级别  0：普通  1：加急  2：紧急 3：特急
	*/
	private Integer emergencyDegree;

	private String emergencyDegreeDesc;

	/**
	* 支付状态  0：未付 1：已付  2：不用付 
	*/
	private Integer payState;

	private String payStateDesc;

	/**
	* 审核状态  0：待审核 1：审核通过  2：审核拒绝
	*/
	private Integer auditState;

	private String auditStateDesc;

	/**
	* 审核时间
	*/
	private Date auditTime;

	/**
	* 审核拒绝原因
	*/
	private String auditReason;

	/**
	* 备注
	*/
	private String remark;

}