package net.maku.excel;


import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class SmartPayDataReport implements Serializable {

    private List<SmartPayDataResult> smartPayDataResult;
    private int successCount;
    private int failedCount;
}
