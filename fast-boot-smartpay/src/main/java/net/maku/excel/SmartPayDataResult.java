package net.maku.excel;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class SmartPayDataResult extends SmartPayTemplateData {

    int resultFlag; //0: 失败 1：成功
    String resultMsg;
}
