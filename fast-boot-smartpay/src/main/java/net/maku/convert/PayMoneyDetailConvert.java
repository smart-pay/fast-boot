package net.maku.convert;

import net.maku.entity.PayMoneyDetailEntity;
import net.maku.vo.PayMoneyDetailVO;
import net.maku.vo.PaymentDetailAddVO;
import net.maku.vo.PaymentDetailEditVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 付款记录明细
*
* @author 中国华 hualaihu@126.com
* @since 1.0.0 2022-10-14
*/
@Mapper
public interface PayMoneyDetailConvert {
    PayMoneyDetailConvert INSTANCE = Mappers.getMapper(PayMoneyDetailConvert.class);

    PayMoneyDetailEntity convert(PayMoneyDetailVO vo);

    PayMoneyDetailEntity convert(PaymentDetailAddVO vo);

    PayMoneyDetailEntity convert(PaymentDetailEditVO vo);

    PayMoneyDetailVO convert(PayMoneyDetailEntity entity);

    List<PayMoneyDetailVO> convertList(List<PayMoneyDetailEntity> list);

}