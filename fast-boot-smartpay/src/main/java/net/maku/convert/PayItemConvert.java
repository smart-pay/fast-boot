package net.maku.convert;

import net.maku.entity.PayedDoneItemEntity;
import net.maku.entity.WillPayItemEntity;
import net.maku.enums.EmergencyDegreeEnum;
import net.maku.enums.PayStateEnum;
import net.maku.excel.PayItemEntity;
import net.maku.excel.SmartPayTemplateData;
import net.maku.vo.PayedDoneItemVO;
import net.maku.vo.WillPayItemAddVO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.util.List;

@Mapper
public interface PayItemConvert {
    PayItemConvert INSTANCE = Mappers.getMapper(PayItemConvert.class);

    PayItemEntity payingConvert(WillPayItemEntity vo);
    List<PayItemEntity> payingConvertList(List<WillPayItemEntity> vo);

    @AfterMapping
    public default void convertPayingEntityAfter(WillPayItemEntity data, @MappingTarget PayItemEntity entity){

        entity.setAuditStateDesc(PayStateEnum.getDescFromValue(data.getAuditState()));

        entity.setPayStateDesc(PayStateEnum.getDescFromValue(data.getPayState()));

        entity.setEmergencyDegreeDesc(EmergencyDegreeEnum.getDescFromValue(data.getEmergencyDegree()));
    }

    PayItemEntity payedConvert(PayedDoneItemEntity vo);
    List<PayItemEntity> payedConvertList(List<PayedDoneItemEntity> vo);

    @AfterMapping
    public default void convertPayedEntityAfter(PayedDoneItemEntity data, @MappingTarget PayItemEntity entity){

        entity.setAuditStateDesc(PayStateEnum.getDescFromValue(data.getAuditState()));

        entity.setPayStateDesc(PayStateEnum.getDescFromValue(data.getPayState()));

        entity.setEmergencyDegreeDesc(EmergencyDegreeEnum.getDescFromValue(data.getEmergencyDegree()));
    }

}
