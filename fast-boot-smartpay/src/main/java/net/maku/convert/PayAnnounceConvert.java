package net.maku.convert;

import net.maku.entity.PayAnnounceEntity;
import net.maku.vo.PayAnnounceEditVO;
import net.maku.vo.PayAnnounceVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 通告
*
* @author 中国华 hualaihu@126.com
* @since 1.0.0 2022-09-15
*/
@Mapper
public interface PayAnnounceConvert {
    PayAnnounceConvert INSTANCE = Mappers.getMapper(PayAnnounceConvert.class);

    PayAnnounceEntity convert(PayAnnounceVO vo);

    PayAnnounceVO convert(PayAnnounceEntity entity);

    List<PayAnnounceVO> convertList(List<PayAnnounceEntity> list);

    PayAnnounceVO updateConvertEntity(PayAnnounceEditVO vo);


}