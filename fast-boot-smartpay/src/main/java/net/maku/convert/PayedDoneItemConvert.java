package net.maku.convert;

import net.maku.entity.PayedDoneItemEntity;
import net.maku.vo.PayedDoneItemVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 已付款管理
*
* @author 中国华 hualaihu@126.com
* @since 1.0-SNAPSHOT 2022-08-29
*/
@Mapper
public interface PayedDoneItemConvert {
    PayedDoneItemConvert INSTANCE = Mappers.getMapper(PayedDoneItemConvert.class);

    PayedDoneItemEntity convert(PayedDoneItemVO vo);

    PayedDoneItemVO convert(PayedDoneItemEntity entity);

    List<PayedDoneItemVO> convertList(List<PayedDoneItemEntity> list);

}