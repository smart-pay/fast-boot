package net.maku.convert;

import net.maku.entity.WillPayItemEntity;
import net.maku.enums.EmergencyDegreeEnum;
import net.maku.enums.PayStateEnum;
import net.maku.excel.SmartPayTemplateData;
import net.maku.vo.*;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.lang.annotation.Target;
import java.math.BigDecimal;
import java.util.List;

/**
* 应付款管理
*
* @author 中国华 hualaihu@126.com
* @since 1.0-SNAPSHOT 2022-08-29
*/
@Mapper
public interface WillPayItemConvert {
    WillPayItemConvert INSTANCE = Mappers.getMapper(WillPayItemConvert.class);

    WillPayItemEntity convert(WillPayItemVO vo);

    WillPayItemEntity addConvertEntity(WillPayItemAddVO vo);

    WillPayItemEntity updateConvertEntity(WillPayItemUpdateVO vo);

    WillPayItemVO convert(WillPayItemEntity entity);

    WillPayItemAddVO excelConvertEntity(SmartPayTemplateData data);

    @AfterMapping
    public default void excelConvertEntityAfter(SmartPayTemplateData data, @MappingTarget WillPayItemAddVO entity){
        entity.setTotalPrice(new BigDecimal(data.getTotalPriceDesc()));

        entity.setPayState(PayStateEnum.getValueFromDesc(data.getPayStateDesc()));

        entity.setEmergencyDegree(EmergencyDegreeEnum.getValueFromDesc(data.getEmergencyDegreeDesc()));
    }

    List<WillPayItemVO> convertList(List<WillPayItemEntity> list);

    List<waitAuditItem> waitAuditConvertList(List<WillPayItemEntity> list);


    List<waitModifyItem> modifyAuditConvertList(List<WillPayItemEntity> list);

    List<WillPayItemEntity> auditConvertList(List<WillPayItemAuditVO> list);

    List<WillPayItemEntity> setDegreeConvertList(List<WillPayItemSetDegreeVO> list);

    List<WillPayItemDashBoardVO> listConvertDashBoard(List<WillPayItemEntity> list);

    WillPayItemUpdateVO editConvertUpdate(WillPayItemEditVO vo);
}