package net.maku.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
* 编辑付款记录
*
* @author 中国华 hualaihu@126.com
* @since 1.0.0 2022-10-14
*/
@Data
@Schema(description = "编辑付款记录")
public class PaymentDetailEditVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Schema(description = "id", required = true)
	@NotNull(message = "id不能为空")
	private Long id;

	@Schema(description = "付款金额", required = true)
	@NotNull(message = "付款金额不能为空")
	private BigDecimal payPrice;
}