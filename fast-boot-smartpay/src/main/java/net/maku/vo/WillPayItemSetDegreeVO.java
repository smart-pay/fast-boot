package net.maku.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
* 应付款管理
*
* @author 中国华 hualaihu@126.com
* @since 1.0-SNAPSHOT 2022-08-29
*/
@Data
@Schema(description = "设置紧急级别")
public class WillPayItemSetDegreeVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Schema(description = "主键id", required = true)
	@NotNull(message = "id不能为空")
	private Long id;

	@Schema(description = "紧急级别  0：普通  1：加急  2：紧急 3：特急", required = true)
	@NotNull(message = "紧急级别不能为空")
	private Integer emergencyDegree;
}