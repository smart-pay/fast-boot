package net.maku.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import net.maku.framework.common.utils.DateUtils;

import java.io.Serializable;
import java.util.Date;

/**
* 通告
*
* @author 中国华 hualaihu@126.com
* @since 1.0.0 2022-09-15
*/
@Data
@Schema(description = "通告")
public class PayAnnounceEditVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Schema(description = "通告id，现阶段值传1")
	private Long id;

	@Schema(description = "内容，最大长度为5")
	private String content;

}