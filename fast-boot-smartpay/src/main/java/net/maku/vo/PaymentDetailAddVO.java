package net.maku.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import net.maku.framework.common.utils.DateUtils;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
* 新增付款记录
*
* @author 中国华 hualaihu@126.com
* @since 1.0.0 2022-10-14
*/
@Data
@Schema(description = "新增付款记录")
public class PaymentDetailAddVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Schema(description = "付款id", required = true)
	@NotNull(message = "付款id不能为空")
	private Long payId;

	@Schema(description = "付款金额", required = true)
	@NotNull(message = "付款金额不能为空")
	private BigDecimal payPrice;
}