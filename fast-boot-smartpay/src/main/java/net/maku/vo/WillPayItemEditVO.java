package net.maku.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
* 应付款管理
*
* @author 中国华 hualaihu@126.com
* @since 1.0-SNAPSHOT 2022-08-29
*/
@Data
@Schema(description = "修改应付款")
public class WillPayItemEditVO implements Serializable {
	private static final long serialVersionUID = 1L;


	@Schema(description = "主键id", required = true)
	@NotNull(message = "id不能为空")
	private Long id;

	@Schema(description = "审批编号")
	private String seq;

	@Schema(description = "项目名称")
	private String projectName;

	@Schema(description = "供应商名称")
	private String supplier;

	@Schema(description = "材料名称")
	private String materialName;

	@Schema(description = "应付款")
	private BigDecimal totalPrice;

	@Schema(description = "紧急级别  0：普通  1：加急  2：紧急 3：特急")
	private Integer emergencyDegree;

	@Schema(description = "支付状态  0：未付 1：已付  2：不用付 ")
	private Integer payState;

	@Schema(description = "备注")
	private String remark;
}