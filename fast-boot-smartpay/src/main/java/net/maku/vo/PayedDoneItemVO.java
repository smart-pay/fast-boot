package net.maku.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.v3.oas.annotations.media.Schema;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.io.Serializable;
import net.maku.framework.common.utils.DateUtils;
import java.math.BigDecimal;
import java.util.Date;

/**
* 已付款管理
*
* @author 中国华 hualaihu@126.com
* @since 1.0-SNAPSHOT 2022-08-29
*/
@Data
@Schema(description = "已付款管理")
public class PayedDoneItemVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Schema(description = "id")
	private Long id;

	@Schema(description = "审批编号")
	private String seq;

	@Schema(description = "项目名称")
	private String projectName;

	@Schema(description = "供应商名称")
	private String supplier;

	@Schema(description = "材料名称")
	private String materialName;

	@Schema(description = "应付款")
	private BigDecimal totalPrice;

	@Schema(description = "紧急级别  0：普通  1：加急  2：紧急 3：特急")
	private Integer emergencyDegree;

	@Schema(description = "支付状态  1：已付  2：不用付 ")
	private Integer payState;

	@Schema(description = "付款时间")
	@JsonFormat(pattern = DateUtils.DATE_TIME_PATTERN)
	private Date payTime;

	@Schema(description = "支付备注")
	private String payRemark;

	@Schema(description = "审核状态 1：审核通过 ")
	private Integer auditState;

	@Schema(description = "审核时间")
	@JsonFormat(pattern = DateUtils.DATE_TIME_PATTERN)
	private Date auditTime;

	@Schema(description = "审核拒绝原因")
	private String auditReason;

	@Schema(description = "备注")
	private String remark;

	@Schema(description = "删除标识  0：正常   1：已删除")
	private Integer deleted;

	@Schema(description = "创建者id")
	private Long creator;

	@Schema(description = "创建者")
	private String  creatorName;

	@Schema(description = "创建时间")
	@JsonFormat(pattern = DateUtils.DATE_TIME_PATTERN)
	private Date createTime;

	@Schema(description = "更新者id")
	private Long updater;

	@Schema(description = "更新者")
	private String updaterName;

	@Schema(description = "更新时间")
	@JsonFormat(pattern = DateUtils.DATE_TIME_PATTERN)
	private Date updateTime;


}