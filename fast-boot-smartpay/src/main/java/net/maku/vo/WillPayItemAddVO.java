package net.maku.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import net.maku.framework.common.utils.DateUtils;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
* 应付款管理
*
* @author 中国华 hualaihu@126.com
* @since 1.0-SNAPSHOT 2022-08-29
*/
@Data
@Schema(description = "新增应付款")
public class WillPayItemAddVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Schema(description = "审批编号", required = true)
	@NotBlank(message = "审批编号不能为空")
	private String seq;

	@Schema(description = "项目名称", required = true)
	@NotBlank(message = "项目名称不能为空")
	private String projectName;

	@Schema(description = "供应商名称", required = true)
	@NotBlank(message = "供应商名称不能为空")
	private String supplier;

	@Schema(description = "材料名称", required = true)
	@NotBlank(message = "材料名称不能为空")
	private String materialName;

	@Schema(description = "应付款", required = true)
	@NotNull(message = "应付款不能为空")
	private BigDecimal totalPrice;

	@Schema(description = "紧急级别  0：普通  1：加急  2：紧急 3：特急", required = true)
	@Range(min = 0, max = 3, message = "紧急级别不正确")
	private Integer emergencyDegree;

	@Schema(description = "支付状态  0：未付 1：已付  2：不用付 ", required = true)
	@Range(min = 0, max = 2, message = "支付状态不正确")
	private Integer payState;

	@Schema(description = "备注")
	private String remark;
}