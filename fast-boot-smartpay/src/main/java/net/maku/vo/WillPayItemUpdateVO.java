package net.maku.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
* 应付款管理
*
* @author 中国华 hualaihu@126.com
* @since 1.0-SNAPSHOT 2022-08-29
*/
@Data
@Schema(description = "纠正")
public class WillPayItemUpdateVO extends WillPayItemEditVO {
	private static final long serialVersionUID = 1L;

	@Schema(description = "审核状态 0：待审核，对待修改时有效")
	private Integer auditState;
}