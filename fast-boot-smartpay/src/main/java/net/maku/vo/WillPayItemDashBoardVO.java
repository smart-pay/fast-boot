package net.maku.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import net.maku.framework.common.utils.DateUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
* 应付款管理
*
* @author 中国华 hualaihu@126.com
* @since 1.0-SNAPSHOT 2022-08-29
*/
@Data
@Schema(description = "应付款管理")
public class WillPayItemDashBoardVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Schema(description = "id")
	private Long id;

	@Schema(description = "审批编号")
	private String seq;

	@Schema(description = "项目名称")
	private String projectName;

	@Schema(description = "供应商名称")
	private String supplier;

	@Schema(description = "材料名称")
	private String materialName;

	@Schema(description = "应付款")
	private BigDecimal totalPrice;

	@Schema(description = "已付款")
	private BigDecimal payedPrice;

	@Schema(description = "紧急级别  0：普通  1：加急  2：紧急 3：特急")
	private Integer emergencyDegree;

	@Schema(description = "备注")
	private String remark;

	@Schema(description = "创建者")
	private Long creator;

	@Schema(description = "创建者id")
	private String  creatorName;

	@Schema(description = "创建时间")
	@JsonFormat(pattern = DateUtils.DATE_TIME_PATTERN)
	private Date createTime;
}