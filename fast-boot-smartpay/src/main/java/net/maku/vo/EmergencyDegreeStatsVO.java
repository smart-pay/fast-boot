package net.maku.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

@Data
@Schema(description = "紧急级别数量")
public class EmergencyDegreeStatsVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @Schema(description = "普通")
    private Integer lowest;

    @Schema(description = "加急")
    private Integer lower;

    @Schema(description = "紧急")
    private Integer higher;

    @Schema(description = "特急")
    private Integer highest;

    @Schema(description = "所有")
    private Integer total;
}
