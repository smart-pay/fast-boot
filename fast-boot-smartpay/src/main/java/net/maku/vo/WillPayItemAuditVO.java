package net.maku.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
* 应付款管理
*
* @author 中国华 hualaihu@126.com
* @since 1.0-SNAPSHOT 2022-08-29
*/
@Data
@Schema(description = "审核")
public class WillPayItemAuditVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Schema(description = "主键id", required = true)
	@NotNull(message = "id不能为空")
	private Long id;

	@Schema(description = "审核状态  1：审核通过  2：审核拒绝", required = true)
	@NotNull(message = "审核状态不能为空")
	private Integer auditState;

	@Schema(description = "审核拒绝原因")
	private String auditReason;
}