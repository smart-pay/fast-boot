package net.maku.dao;

import net.maku.framework.common.dao.BaseDao;
import net.maku.entity.PayAnnounceEntity;
import org.apache.ibatis.annotations.Mapper;

/**
* 通告
*
* @author 中国华 hualaihu@126.com
* @since 1.0.0 2022-09-15
*/
@Mapper
public interface PayAnnounceDao extends BaseDao<PayAnnounceEntity> {
	
}