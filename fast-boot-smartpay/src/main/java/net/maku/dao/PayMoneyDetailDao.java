package net.maku.dao;

import net.maku.framework.common.dao.BaseDao;
import net.maku.entity.PayMoneyDetailEntity;
import org.apache.ibatis.annotations.Mapper;

/**
* 付款记录明细
*
* @author 中国华 hualaihu@126.com
* @since 1.0.0 2022-10-14
*/
@Mapper
public interface PayMoneyDetailDao extends BaseDao<PayMoneyDetailEntity> {
	
}