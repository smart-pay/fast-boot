package net.maku.dao;

import net.maku.framework.common.dao.BaseDao;
import net.maku.entity.PayedDoneItemEntity;
import org.apache.ibatis.annotations.Mapper;

/**
* 已付款管理
*
* @author 中国华 hualaihu@126.com
* @since 1.0-SNAPSHOT 2022-08-29
*/
@Mapper
public interface PayedDoneItemDao extends BaseDao<PayedDoneItemEntity> {

    void moveToWillPayItem(Long id);

    void deletePayedDoneItem(Long id);

    void rebackToWillPayItem(Long id);
}