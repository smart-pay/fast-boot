package net.maku.dao;

import net.maku.framework.common.dao.BaseDao;
import net.maku.entity.WillPayItemEntity;
import net.maku.vo.EmergencyDegreeStatsVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* 应付款管理
*
* @author 中国华 hualaihu@126.com
* @since 1.0-SNAPSHOT 2022-08-29
*/
@Mapper
public interface WillPayItemDao extends BaseDao<WillPayItemEntity> {

    int moveToPayDoneItem(List<Long> ids);

    int deletePaylessItem(List<Long> ids);

    EmergencyDegreeStatsVO statsEmergencyDegree();
}