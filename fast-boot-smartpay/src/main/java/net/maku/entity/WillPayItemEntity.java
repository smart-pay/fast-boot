package net.maku.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.annotation.*;
import java.math.BigDecimal;
import java.util.Date;
import net.maku.framework.common.entity.BaseEntity;
import net.maku.framework.common.entity.NameBaseEntity;

/**
 * 应付款管理
 *
 * @author 中国华 hualaihu@126.com
 * @since 1.0-SNAPSHOT 2022-08-29
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("pay_will_item")
public class WillPayItemEntity extends NameBaseEntity {
	/**
	* 审批编号
	*/
	@TableField(updateStrategy= FieldStrategy.NOT_EMPTY)
	private String seq;

	/**
	* 项目名称
	*/
	@TableField(updateStrategy= FieldStrategy.NOT_EMPTY)
	private String projectName;

	/**
	* 供应商名称
	*/
	@TableField(updateStrategy= FieldStrategy.NOT_EMPTY)
	private String supplier;

	/**
	* 材料名称
	*/
	@TableField(updateStrategy= FieldStrategy.NOT_EMPTY)
	private String materialName;

	/**
	* 应付款
	*/
	@TableField(updateStrategy= FieldStrategy.NOT_NULL)
	private BigDecimal totalPrice;

	/**
	 * 实际已付款金额
	 */
	@TableField(updateStrategy= FieldStrategy.NOT_NULL)
	private BigDecimal payedPrice;

	/**
	* 紧急级别  0：普通  1：加急  2：紧急 3：特急
	*/
	@TableField(updateStrategy= FieldStrategy.NOT_NULL)
	private Integer emergencyDegree;


	/**
	* 支付状态  0：未付 1：已付  2：不用付 
	*/
	@TableField(updateStrategy= FieldStrategy.NOT_NULL)
	private Integer payState;

	/**
	* 审核状态  0：待审核 1：审核通过  2：审核拒绝
	*/
	@TableField(updateStrategy= FieldStrategy.NOT_NULL)
	private Integer auditState;

	/**
	* 审核时间
	*/
	@TableField(updateStrategy= FieldStrategy.NOT_NULL)
	private Date auditTime;

	/**
	* 审核拒绝原因
	*/
	@TableField(updateStrategy= FieldStrategy.NOT_EMPTY)
	private String auditReason;

	/**
	* 备注
	*/
	@TableField(updateStrategy= FieldStrategy.NOT_EMPTY)
	private String remark;

}