package net.maku.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.annotation.*;
import java.util.Date;

/**
 * 通告
 *
 * @author 中国华 hualaihu@126.com
 * @since 1.0.0 2022-09-15
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("pay_announce")
public class PayAnnounceEntity {
	/**
	* id
	*/
	@TableId
	private Long id;

	/**
	* 备注
	*/
	private String content;

	/**
	* 删除标识  0：正常   1：已删除
	*/
	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	private Integer deleted;

	/**
	* 创建者
	*/
	@TableField(fill = FieldFill.INSERT)
	private Long creator;

	/**
	* 创建者姓名
	*/
	private String creatorName;

	/**
	* 创建时间
	*/
	@TableField(fill = FieldFill.INSERT)
	private Date createTime;

	/**
	* 更新者
	*/
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Long updater;

	/**
	* 更新者姓名
	*/
	private String updaterName;

	/**
	* 更新时间
	*/
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateTime;

	private Integer version;

}