package net.maku.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.annotation.*;
import java.math.BigDecimal;
import java.util.Date;
import net.maku.framework.common.entity.BaseEntity;
import net.maku.framework.common.entity.NameBaseEntity;

/**
 * 付款记录明细
 *
 * @author 中国华 hualaihu@126.com
 * @since 1.0.0 2022-10-14
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("pay_money_detail")
public class PayMoneyDetailEntity extends NameBaseEntity {
	/**
	* 付款id
	*/
	private Long payId;

	/**
	* 付款金额
	*/
	private BigDecimal payPrice;

}