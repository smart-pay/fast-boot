package net.maku.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.baomidou.mybatisplus.annotation.*;
import java.math.BigDecimal;
import java.util.Date;
import net.maku.framework.common.entity.BaseEntity;
import net.maku.framework.common.entity.NameBaseEntity;

/**
 * 已付款管理
 *
 * @author 中国华 hualaihu@126.com
 * @since 1.0-SNAPSHOT 2022-08-29
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("pay_done_item")
public class PayedDoneItemEntity extends NameBaseEntity {
	/**
	* 审批编号
	*/
	private String seq;

	/**
	* 项目名称
	*/
	private String projectName;

	/**
	* 供应商名称
	*/
	private String supplier;

	/**
	* 材料名称
	*/
	private String materialName;

	/**
	* 应付款
	*/
	private BigDecimal totalPrice;

	/**
	 * 实际已付款金额
	 */
	@TableField(updateStrategy= FieldStrategy.NOT_NULL)
	private BigDecimal payedPrice;

	/**
	* 紧急级别  0：普通  1：加急  2：紧急 3：特急
	*/
	private Integer emergencyDegree;

	/**
	* 支付状态  1：已付  2：不用付 
	*/
	private Integer payState;

	/**
	* 付款时间
	*/
	private Date payTime;

	/**
	* 支付备注
	*/
	private String payRemark;

	/**
	* 审核状态 1：审核通过 
	*/
	private Integer auditState;

	/**
	* 审核时间
	*/
	private Date auditTime;

	/**
	* 审核拒绝原因
	*/
	private String auditReason;

	/**
	* 备注
	*/
	private String remark;

}