package net.maku.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import net.maku.entity.WillPayItemEntity;
import net.maku.enums.AuditStateEnum;
import net.maku.enums.PayStateEnum;
import net.maku.framework.common.page.PageResult;
import net.maku.framework.common.service.impl.BaseServiceImpl;
import net.maku.convert.PayedDoneItemConvert;
import net.maku.entity.PayedDoneItemEntity;
import net.maku.query.PayedDoneItemQuery;
import net.maku.vo.PayedDoneItemVO;
import net.maku.dao.PayedDoneItemDao;
import net.maku.service.PayedDoneItemService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 已付款管理
 *
 * @author 中国华 hualaihu@126.com
 * @since 1.0-SNAPSHOT 2022-08-29
 */
@Service
@AllArgsConstructor
public class PayedDoneItemServiceImpl extends BaseServiceImpl<PayedDoneItemDao, PayedDoneItemEntity> implements PayedDoneItemService {

    @Override
    public PageResult<PayedDoneItemVO> page(PayedDoneItemQuery query) {
        IPage<PayedDoneItemEntity> page = baseMapper.selectPage(getPage(query), getWrapper(query));

        return new PageResult<>(PayedDoneItemConvert.INSTANCE.convertList(page.getRecords()), page.getTotal());
    }

    private LambdaQueryWrapper<PayedDoneItemEntity> getWrapper(PayedDoneItemQuery query){
        LambdaQueryWrapper<PayedDoneItemEntity> wrapper = Wrappers.lambdaQuery();

        wrapper.ge(query.getMinTotalPrice() != null, PayedDoneItemEntity::getTotalPrice, query.getMinTotalPrice())
                .le(query.getMaxTotalPrice() != null, PayedDoneItemEntity::getTotalPrice, query.getMaxTotalPrice())
                .ge(query.getStartPayedTime() != null, PayedDoneItemEntity::getPayTime, query.getStartPayedTime())
                .le(query.getEndPayedTime() != null, PayedDoneItemEntity::getPayTime, query.getEndPayedTime());

        if (StringUtils.hasText(query.getKeyword())){
            //查询审批编号或项目名称或供应商名称或者材料名称
            wrapper.and(qw -> qw.like( PayedDoneItemEntity::getSeq, query.getKeyword()).or()
                    .like(PayedDoneItemEntity::getProjectName, query.getKeyword()).or()
                    .like(PayedDoneItemEntity::getSupplier, query.getKeyword()).or()
                    .like(PayedDoneItemEntity::getMaterialName, query.getKeyword()));
        }



        dataScopeWrapper(wrapper);

        return wrapper;
    }

    @Override
    public void save(PayedDoneItemVO vo) {
        PayedDoneItemEntity entity = PayedDoneItemConvert.INSTANCE.convert(vo);

        baseMapper.insert(entity);
    }

    @Override
    public void update(PayedDoneItemVO vo) {
        PayedDoneItemEntity entity = PayedDoneItemConvert.INSTANCE.convert(vo);

        updateById(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> idList) {
        removeByIds(idList);
    }

    @Override
    public void setUnpay(Long id) {
        //更新为已付款
        LambdaUpdateWrapper<PayedDoneItemEntity> wrapper = Wrappers.lambdaUpdate();
        wrapper.set(PayedDoneItemEntity::getPayState, PayStateEnum.PAY_WAIT.getValue())
                .eq(PayedDoneItemEntity::getId, id);
        update(wrapper);

        //需要将记录移到will_pay_item
        baseMapper.moveToWillPayItem(id);

        //删除已付款记录
        baseMapper.deletePayedDoneItem(id);
    }

    @Override
    public void setReback(Long id) {

        //恢复到待审核状态
        baseMapper.rebackToWillPayItem(id);

        //删除已付款记录
        baseMapper.deletePayedDoneItem(id);
    }

}