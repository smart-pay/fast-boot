package net.maku.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import net.maku.entity.WillPayItemEntity;
import net.maku.framework.common.page.PageResult;
import net.maku.framework.common.service.impl.BaseServiceImpl;
import net.maku.convert.PayMoneyDetailConvert;
import net.maku.entity.PayMoneyDetailEntity;
import net.maku.query.PayMoneyDetailQuery;
import net.maku.service.PayedDoneItemService;
import net.maku.service.WillPayItemService;
import net.maku.vo.PayMoneyDetailVO;
import net.maku.dao.PayMoneyDetailDao;
import net.maku.service.PayMoneyDetailService;
import net.maku.vo.PaymentDetailAddVO;
import net.maku.vo.PaymentDetailEditVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 付款记录明细
 *
 * @author 中国华 hualaihu@126.com
 * @since 1.0.0 2022-10-14
 */
@Service
@AllArgsConstructor
public class PayMoneyDetailServiceImpl extends BaseServiceImpl<PayMoneyDetailDao, PayMoneyDetailEntity> implements PayMoneyDetailService {

    private final WillPayItemService willPayItemService;

    @Override
    public PageResult<PayMoneyDetailVO> page(PayMoneyDetailQuery query) {
        IPage<PayMoneyDetailEntity> page = baseMapper.selectPage(getPage(query), getWrapper(query));

        return new PageResult<>(PayMoneyDetailConvert.INSTANCE.convertList(page.getRecords()), page.getTotal());
    }

    private LambdaQueryWrapper<PayMoneyDetailEntity> getWrapper(PayMoneyDetailQuery query){
        LambdaQueryWrapper<PayMoneyDetailEntity> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(PayMoneyDetailEntity::getPayId, query.getPayId());
        return wrapper;
    }

    @Override
    public void save(PayMoneyDetailVO vo) {
        PayMoneyDetailEntity entity = PayMoneyDetailConvert.INSTANCE.convert(vo);

        baseMapper.insert(entity);
    }

    @Override
    public void update(PayMoneyDetailVO vo) {
        PayMoneyDetailEntity entity = PayMoneyDetailConvert.INSTANCE.convert(vo);

        updateById(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void savePayDetail(PaymentDetailAddVO vo) {

        willPayItemService.setPayedPrice(vo.getPayId(), vo.getPayPrice());

        this.save(PayMoneyDetailConvert.INSTANCE.convert(vo));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updatePayDetail(PaymentDetailEditVO vo) {

        PayMoneyDetailEntity paymentDetail = this.getById(vo.getId());

        willPayItemService.setPayedPrice(paymentDetail.getPayId(), vo.getPayPrice().subtract(paymentDetail.getPayPrice()));

        LambdaUpdateWrapper<PayMoneyDetailEntity> wrapper = Wrappers.lambdaUpdate();
        wrapper.set(PayMoneyDetailEntity::getPayPrice, vo.getPayPrice())
                .eq(PayMoneyDetailEntity::getId, vo.getId());

        update(wrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deletePayDetail(Long id) {

        PayMoneyDetailEntity paymentDetail = this.getById(id);

        willPayItemService.setPayedPrice(paymentDetail.getPayId(), paymentDetail.getPayPrice().negate());

        this.delete(Arrays.asList(id));
    }
}