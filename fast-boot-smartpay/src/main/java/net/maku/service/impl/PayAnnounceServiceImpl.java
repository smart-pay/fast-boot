package net.maku.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import net.maku.framework.common.page.PageResult;
import net.maku.framework.common.service.impl.BaseServiceImpl;
import net.maku.convert.PayAnnounceConvert;
import net.maku.entity.PayAnnounceEntity;
import net.maku.query.PayAnnounceQuery;
import net.maku.vo.PayAnnounceVO;
import net.maku.dao.PayAnnounceDao;
import net.maku.service.PayAnnounceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 通告
 *
 * @author 中国华 hualaihu@126.com
 * @since 1.0.0 2022-09-15
 */
@Service
@AllArgsConstructor
public class PayAnnounceServiceImpl extends BaseServiceImpl<PayAnnounceDao, PayAnnounceEntity> implements PayAnnounceService {

    @Override
    public PageResult<PayAnnounceVO> page(PayAnnounceQuery query) {
        IPage<PayAnnounceEntity> page = baseMapper.selectPage(getPage(query), getWrapper(query));

        return new PageResult<>(PayAnnounceConvert.INSTANCE.convertList(page.getRecords()), page.getTotal());
    }

    private LambdaQueryWrapper<PayAnnounceEntity> getWrapper(PayAnnounceQuery query){
        LambdaQueryWrapper<PayAnnounceEntity> wrapper = Wrappers.lambdaQuery();

        return wrapper;
    }

    @Override
    public void save(PayAnnounceVO vo) {
        PayAnnounceEntity entity = PayAnnounceConvert.INSTANCE.convert(vo);

        baseMapper.insert(entity);
    }

    @Override
    public void update(PayAnnounceVO vo) {
        PayAnnounceEntity entity = PayAnnounceConvert.INSTANCE.convert(vo);

        updateById(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> idList) {
        removeByIds(idList);
    }

}