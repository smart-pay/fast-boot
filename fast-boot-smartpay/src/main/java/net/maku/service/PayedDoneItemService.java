package net.maku.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import net.maku.entity.WillPayItemEntity;
import net.maku.framework.common.page.PageResult;
import net.maku.framework.common.service.BaseService;
import net.maku.vo.PayedDoneItemVO;
import net.maku.query.PayedDoneItemQuery;
import net.maku.entity.PayedDoneItemEntity;

import java.util.List;

/**
 * 已付款管理
 *
 * @author 中国华 hualaihu@126.com
 * @since 1.0-SNAPSHOT 2022-08-29
 */
public interface PayedDoneItemService extends BaseService<PayedDoneItemEntity> {

    PageResult<PayedDoneItemVO> page(PayedDoneItemQuery query);

    void save(PayedDoneItemVO vo);

    default PayedDoneItemEntity getBySeq(String seq){
        LambdaQueryWrapper<PayedDoneItemEntity> wrapper = Wrappers.lambdaQuery();
        return getOne(wrapper.eq(PayedDoneItemEntity::getSeq, seq));
    }

    void update(PayedDoneItemVO vo);

    void delete(List<Long> idList);

    void setUnpay(Long id);

    void setReback(Long id);
}