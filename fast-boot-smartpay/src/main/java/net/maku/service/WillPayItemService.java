package net.maku.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import net.maku.framework.common.page.PageResult;
import net.maku.framework.common.service.BaseService;
import net.maku.query.WillPayItemAuditQuery;
import net.maku.query.WillPayItemDashBoardQuery;
import net.maku.query.WillPayItemModifyQuery;
import net.maku.vo.*;
import net.maku.query.WillPayItemQuery;
import net.maku.entity.WillPayItemEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;

/**
 * 应付款管理
 *
 * @author 中国华 hualaihu@126.com
 * @since 1.0-SNAPSHOT 2022-08-29
 */
public interface WillPayItemService extends BaseService<WillPayItemEntity> {

    PageResult<WillPayItemVO> page(WillPayItemQuery query);

    PageResult<waitAuditItem> auditPage(WillPayItemAuditQuery query);

    PageResult<waitModifyItem> modifyPage(WillPayItemModifyQuery query);

    void save(WillPayItemAddVO vo);

    void update(WillPayItemUpdateVO vo);

    void delete(List<Long> idList);

    default WillPayItemEntity getBySeq(String seq){
        LambdaQueryWrapper<WillPayItemEntity> wrapper = Wrappers.lambdaQuery();
        return getOne(wrapper.eq(WillPayItemEntity::getSeq, seq));
    }

    void auditBatch(List<WillPayItemAuditVO> vo);


    void batchSetDegree(List<WillPayItemSetDegreeVO> vo);

    void batchSetPayed(List<Long> ids);

    void setPayedPrice(Long id, BigDecimal payPrice);

    EmergencyDegreeStatsVO statsEmergencyDegree();

    List<WillPayItemDashBoardVO> dashboard(WillPayItemDashBoardQuery query);

    String importExcel(MultipartFile file);

    void exportAll(HttpServletResponse response);

    void setReback(Long id);
}