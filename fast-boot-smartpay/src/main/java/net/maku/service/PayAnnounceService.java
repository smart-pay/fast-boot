package net.maku.service;

import net.maku.framework.common.page.PageResult;
import net.maku.framework.common.service.BaseService;
import net.maku.vo.PayAnnounceVO;
import net.maku.query.PayAnnounceQuery;
import net.maku.entity.PayAnnounceEntity;

import java.util.List;

/**
 * 通告
 *
 * @author 中国华 hualaihu@126.com
 * @since 1.0.0 2022-09-15
 */
public interface PayAnnounceService extends BaseService<PayAnnounceEntity> {

    PageResult<PayAnnounceVO> page(PayAnnounceQuery query);

    void save(PayAnnounceVO vo);

    void update(PayAnnounceVO vo);

    void delete(List<Long> idList);
}