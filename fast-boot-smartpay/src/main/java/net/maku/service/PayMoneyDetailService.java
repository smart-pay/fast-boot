package net.maku.service;

import net.maku.framework.common.page.PageResult;
import net.maku.framework.common.service.BaseService;
import net.maku.vo.PayMoneyDetailVO;
import net.maku.query.PayMoneyDetailQuery;
import net.maku.entity.PayMoneyDetailEntity;
import net.maku.vo.PaymentDetailAddVO;
import net.maku.vo.PaymentDetailEditVO;

import java.util.List;

/**
 * 付款记录明细
 *
 * @author 中国华 hualaihu@126.com
 * @since 1.0.0 2022-10-14
 */
public interface PayMoneyDetailService extends BaseService<PayMoneyDetailEntity> {

    PageResult<PayMoneyDetailVO> page(PayMoneyDetailQuery query);

    void save(PayMoneyDetailVO vo);

    void update(PayMoneyDetailVO vo);

    void delete(List<Long> idList);

    void savePayDetail(PaymentDetailAddVO vo);

    void updatePayDetail(PaymentDetailEditVO vo);

    void deletePayDetail(Long id);
}