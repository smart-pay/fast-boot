package net.maku.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import net.maku.framework.common.page.PageResult;
import net.maku.framework.common.utils.Result;
import net.maku.convert.PayMoneyDetailConvert;
import net.maku.entity.PayMoneyDetailEntity;
import net.maku.service.PayMoneyDetailService;
import net.maku.query.PayMoneyDetailQuery;
import net.maku.vo.PayMoneyDetailVO;
import net.maku.vo.PaymentDetailAddVO;
import net.maku.vo.PaymentDetailEditVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
* 付款记录明细
*
* @author 中国华 hualaihu@126.com
* @since 1.0.0 2022-10-14
*/
@RestController
@RequestMapping("paymentdetail")
@Tag(name="付款记录明细")
@AllArgsConstructor
public class PayMoneyDetailController {
    private final PayMoneyDetailService payMoneyDetailService;

    @PostMapping("page")
    @Operation(summary = "分页")
    @PreAuthorize("hasAuthority('paymoneydetail:page')")
    public Result<PageResult<PayMoneyDetailVO>> page(@RequestBody @Valid PayMoneyDetailQuery query){
        PageResult<PayMoneyDetailVO> page = payMoneyDetailService.page(query);

        return Result.ok(page);
    }

    @GetMapping("{id}")
    @Operation(summary = "信息")
    @PreAuthorize("hasAuthority('paymoneydetail:info')")
    public Result<PayMoneyDetailVO> get(@PathVariable("id") Long id){
        PayMoneyDetailEntity entity = payMoneyDetailService.getById(id);

        return Result.ok(PayMoneyDetailConvert.INSTANCE.convert(entity));
    }

    @PostMapping
    @Operation(summary = "保存")
    @PreAuthorize("hasAuthority('paymoneydetail:save')")
    public Result<String> save(@RequestBody @Valid PaymentDetailAddVO vo){

        payMoneyDetailService.savePayDetail(vo);

        return Result.ok();
    }

    @PutMapping
    @Operation(summary = "修改")
    @PreAuthorize("hasAuthority('paymoneydetail:update')")
    public Result<String> update(@RequestBody @Valid PaymentDetailEditVO vo){
        payMoneyDetailService.updatePayDetail(vo);

        return Result.ok();
    }

    @DeleteMapping
    @Operation(summary = "删除")
    @PreAuthorize("hasAuthority('paymoneydetail:delete')")
    public Result<String> delete(@RequestBody Long id){
        payMoneyDetailService.deletePayDetail(id);

        return Result.ok();
    }
}