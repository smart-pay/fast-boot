package net.maku.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import net.maku.framework.common.page.PageResult;
import net.maku.framework.common.utils.Result;
import net.maku.convert.PayedDoneItemConvert;
import net.maku.entity.PayedDoneItemEntity;
import net.maku.service.PayedDoneItemService;
import net.maku.query.PayedDoneItemQuery;
import net.maku.vo.PayedDoneItemVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
* 已付款管理
*
* @author 中国华 hualaihu@126.com
* @since 1.0-SNAPSHOT 2022-08-29
*/
@RestController
@RequestMapping("payed/done")
@Tag(name="已付款管理")
@AllArgsConstructor
public class PayedDoneItemController {
    private final PayedDoneItemService payedDoneItemService;

    @GetMapping("page")
    @Operation(summary = "已付款分页")
    @PreAuthorize("hasAuthority('payeddoneitem:page')")
    public Result<PageResult<PayedDoneItemVO>> page(@Valid PayedDoneItemQuery query){
        PageResult<PayedDoneItemVO> page = payedDoneItemService.page(query);

        return Result.ok(page);
    }

    @GetMapping("{id}")
    @Operation(summary = "信息")
    @PreAuthorize("hasAuthority('payeddoneitem:info')")
    public Result<PayedDoneItemVO> get(@PathVariable("id") Long id){
        PayedDoneItemEntity entity = payedDoneItemService.getById(id);

        return Result.ok(PayedDoneItemConvert.INSTANCE.convert(entity));
    }

    /*
    @PostMapping
    @Operation(summary = "保存")
    @PreAuthorize("hasAuthority('payeddoneitem:save')")
    public Result<String> save(@RequestBody PayedDoneItemVO vo){
        payedDoneItemService.save(vo);

        return Result.ok();
    }

    @PutMapping
    @Operation(summary = "修改")
    @PreAuthorize("hasAuthority('payeddoneitem:update')")
    public Result<String> update(@RequestBody @Valid PayedDoneItemVO vo){
        payedDoneItemService.update(vo);

        return Result.ok();
    }
    */
    @DeleteMapping
    @Operation(summary = "删除")
    @PreAuthorize("hasAuthority('payeddoneitem:delete')")
    public Result<String> delete(@RequestBody List<Long> idList){
        payedDoneItemService.delete(idList);

        return Result.ok();
    }

    @PutMapping("/{id}")
    @Operation(summary = "修改为未付款")
    @PreAuthorize("hasAuthority('payeddoneitem:setunpay')")
    public Result<String> setUnpay(@PathVariable("id") Long id){
        payedDoneItemService.setUnpay(id);

        return Result.ok();
    }

    /*
    @PutMapping("/reback/{id}")
    @Operation(summary = "撤销")
    @PreAuthorize("hasAuthority('payeddoneitem:setReback')")
    public Result<String> setReback(@PathVariable("id") Long id){
        payedDoneItemService.setReback(id);

        return Result.ok();
    }*/
}