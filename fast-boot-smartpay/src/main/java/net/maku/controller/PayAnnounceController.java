package net.maku.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import net.maku.framework.common.page.PageResult;
import net.maku.framework.common.utils.Result;
import net.maku.convert.PayAnnounceConvert;
import net.maku.entity.PayAnnounceEntity;
import net.maku.service.PayAnnounceService;
import net.maku.query.PayAnnounceQuery;
import net.maku.vo.PayAnnounceEditVO;
import net.maku.vo.PayAnnounceVO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
* 通告
*
* @author 中国华 hualaihu@126.com
* @since 1.0.0 2022-09-15
*/
@RestController
@RequestMapping("pay/announce")
@Tag(name="通告")
@AllArgsConstructor
public class PayAnnounceController {
    private final PayAnnounceService payAnnounceService;

   /* @GetMapping("page")
    @Operation(summary = "分页")
    @PreAuthorize("hasAuthority('payannounce:page')")
    public Result<PageResult<PayAnnounceVO>> page(@Valid PayAnnounceQuery query){
        PageResult<PayAnnounceVO> page = payAnnounceService.page(query);

        return Result.ok(page);
    }*/

    @GetMapping("{id}")
    @Operation(summary = "信息")
    @Parameter(name = "id", description = "通告id，现阶段值传1", required = true)
    @PreAuthorize("hasAuthority('payannounce:info')")
    public Result<PayAnnounceVO> get(@PathVariable("id") Long id){
        PayAnnounceEntity entity = payAnnounceService.getById(id);

        return Result.ok(PayAnnounceConvert.INSTANCE.convert(entity));
    }

    /*
    @PostMapping
    @Operation(summary = "保存")
    @PreAuthorize("hasAuthority('payannounce:save')")
    public Result<String> save(@RequestBody PayAnnounceVO vo){
        payAnnounceService.save(vo);

        return Result.ok();
    }*/

    @PutMapping
    @Operation(summary = "修改")
    @PreAuthorize("hasAuthority('payannounce:update')")
    public Result<String> update(@RequestBody @Valid PayAnnounceEditVO vo){
        PayAnnounceVO payAnnounceVO = PayAnnounceConvert.INSTANCE.updateConvertEntity(vo);
        payAnnounceService.update(payAnnounceVO);

        return Result.ok();
    }

    /*
    @DeleteMapping
    @Operation(summary = "删除")
    @PreAuthorize("hasAuthority('payannounce:delete')")
    public Result<String> delete(@RequestBody List<Long> idList){
        payAnnounceService.delete(idList);

        return Result.ok();
    }*/
}