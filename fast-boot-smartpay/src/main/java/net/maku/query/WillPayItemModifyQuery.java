package net.maku.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.maku.framework.common.query.Query;

/**
* 应付款管理查询
*
* @author 中国华 hualaihu@126.com
* @since 1.0-SNAPSHOT 2022-08-29
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(description = "待修改查询")
public class WillPayItemModifyQuery extends Query {

    @Schema(description = "关键字：审批编号或项目名称或供应商名称或者材料名称")
    private String keyword;
}