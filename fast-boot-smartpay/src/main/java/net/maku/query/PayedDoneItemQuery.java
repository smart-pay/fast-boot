package net.maku.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.maku.framework.common.query.Query;
import net.maku.framework.common.utils.DateUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
* 已付款管理查询
*
* @author 中国华 hualaihu@126.com
* @since 1.0-SNAPSHOT 2022-08-29
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(description = "已付款管理查询")
public class PayedDoneItemQuery extends Query {
    @Schema(description = "关键字：审批编号或项目名称或供应商名称或者材料名称")
    private String keyword;

    @Schema(description = "最小应付款")
    private BigDecimal minTotalPrice;

    @Schema(description = "最大应付款")
    private BigDecimal maxTotalPrice;

    @Schema(description = "开始付款时间")
    @DateTimeFormat(pattern = DateUtils.DATE_TIME_PATTERN)
    private Date startPayedTime;

    @Schema(description = "结束付款时间")
    @DateTimeFormat(pattern = DateUtils.DATE_TIME_PATTERN)
    private Date endPayedTime;

    @Schema(description = "排序字段： emergency_degree或create_time或pay_time")
    String order;
}