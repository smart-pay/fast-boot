package net.maku.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.maku.framework.common.query.Query;
import net.maku.framework.common.utils.DateUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
* 应付款管理查询
*
* @author 中国华 hualaihu@126.com
* @since 1.0-SNAPSHOT 2022-08-29
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(description = "应付款查询")
public class WillPayItemQuery extends Query {
    @Schema(description = "关键字：审批编号或项目名称或供应商名称或者材料名称")
    private String keyword;

    @Schema(description = "最小应付款")
    private BigDecimal minTotalPrice;

    @Schema(description = "最大应付款")
    private BigDecimal maxTotalPrice;

    @Schema(description = "紧急级别  0：普通  1：加急  2：紧急 3：特急")
    private List<Integer> emergencyDegrees;

    @Schema(description = "开始创建时间")
    @DateTimeFormat(pattern = DateUtils.DATE_TIME_PATTERN)
    private Date startCreateTime;

    @Schema(description = "结束创建时间")
    @DateTimeFormat(pattern = DateUtils.DATE_TIME_PATTERN)
    private Date endCreateTime;

    @Schema(description = "排序字段： emergency_degree或create_time")
    String order;
}