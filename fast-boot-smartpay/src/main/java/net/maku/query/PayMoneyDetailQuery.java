package net.maku.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.maku.framework.common.query.Query;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
* 付款记录明细查询
*
* @author 中国华 hualaihu@126.com
* @since 1.0.0 2022-10-14
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(description = "付款记录明细查询")
public class PayMoneyDetailQuery extends Query {
    @Schema(description = "应付款id", required = true)
    private Long payId;
}