package net.maku.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import net.maku.framework.common.query.Query;

import java.util.Date;

/**
* 通告查询
*
* @author 中国华 hualaihu@126.com
* @since 1.0.0 2022-09-15
*/
@Data
@EqualsAndHashCode(callSuper = false)
@Schema(description = "通告查询")
public class PayAnnounceQuery extends Query {
}